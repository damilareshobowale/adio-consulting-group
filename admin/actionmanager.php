<?php

//require_once("inc_dbfunctions.php");
require_once("config.php");

$actionmanager = New ActionManager();

if(isset($_POST['command']) && $_POST['command'] == 'apply')
{
    $actionmanager->apply();
}
elseif(isset($_POST['command']) && $_POST['command'] == 'login')
{
    $actionmanager->login();
}

/**
 * 
 */
class ActionManager
{
	
	function apply()
	{
		$firstname = $_POST['firstname'];
		$surname = $_POST['surname'];
		$phonenumber = $_POST['phonenumber'];
		$email = $_POST['email'];
		$coverletter = $_POST['coverletter'];
		$passport = $_FILES['passport'];
		$resume = $_FILES['resume'];

		$mycon = databaseConnect();
		$dataRead = New DataRead();
		$dataWrite = New DataWrite();
		//check if the application is more than 4
		$application_count = $dataRead->application_count($mycon);
		if ($application_count >= 4)
		{
			echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> Application Closed</div>
                    <script type='text/javascript'>
                    	$('#status').html('Application Closed');
                    </script>";
            return;
		}
		
				$valid_mime_types = array(
    "image/jpeg",
    "application/pdf",
    "application/msword",
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
);
		
		//check if the user has applied before
		$applicationstatus = $dataRead->application_getbyemail($mycon, $email);
		if ($applicationstatus)
		{
			echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> Sorry, you have already applied. Thank you.</div>";
            return;
		}



		$mycon->beginTransaction();
		$msg = '';
		//check the validations of the files
		if ((($passport['size'] < 100000 && in_array($passport['type'], $valid_mime_types))) && ($resume['size'] > 250000 && in_array($passport['type'], $valid_mime_types) || in_array($passport['type'], $valid_mime_types) ||in_array($passport['type'], $valid_mime_types)))
		{
			//add the applications
			$application_add = $dataWrite->application_add($mycon, $firstname, $surname, $phonenumber, $email, $coverletter, $passport['name'], $resume['name']);
			if (!$application_add)
			{
				echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> There was an error updating your applications. Please try later.</div>";
                 $mycon->rollBack();
                 return;
			}
			//push the files upload to their respective folder
			 move_uploaded_file($passport['tmp_name'],"../passport/".$passport['name']);
			move_uploaded_file($resume['tmp_name'],"../resume/".$resume['name']);

			//if all is all done then commit
			$mycon->commit();
			echo "<div class='alert alert-success alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-check'></i> Application Successful. Thank you!</div>";
            return;
		}
		else
		{
			echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> Please try upload the required passport and resume</div>";
            $mycon->rollBack();
            return;
		}

		return;

	}


	//request course online
	function request_course()
	{
		$name = $_POST['name'];
		$email = $_POST['email'];
		$phonenumber = $_POST['phonenumber'];
		$course = $_POST['course'];

		//send the message
		$message = "<div>
						<p> Hello admin, ".$name." request for a course from the website. Please attend to it. </p><br>
						<p>Name: ". $name."</p>
						<p>Email: ".$email."</p>
						<p>Phone Number: ".$phonenumber."</p>
						<p>Course Requested: ".$course."</p>
						<br><br>
						Thank you Admin.
					</div>";
		$replymessage = "<div>
							<p> Hello ".$name." Your request has been received. We will definitely get back to you as soon as possible. If by any means we didn't get back to you, please ensure you call us through <strong> +234 703 243 6335.</strong> </p>
							<br><br>
							Thank You for contacting us. <br>
							Launchi Software Academy
							</div>";

		//send the message
		if(sendEmail('launchisoftwareacademy@gmail.com', 'New Course Request | Launchi Software Academy', $message) && sendEmail($email, 'Dear '.$name.', Your Request has been delivered', $replymessage))
		{
			echo "<span style='color: green;'> Your Message Delivered Successfully. Please check your Inbox. We will get back to you as soon as possible.</span>
				<script type='text/javascript'>
					$('#name').val();
					$('#email').val();
					$('#phonenumber').val();
					$('#message').val();
				</script>";
			return;
		}

	else 
	{
		echo "<span style='color: #ff0000;'> An error occured while sending message, please try again. </span>";
		return;
		
	}

	//also send the message to the applicants

	return;

	}

	function login()
    {
        $mycon = databaseConnect();
        $username = $_POST['username'];
        $password = $_POST['password'];
        $thedate = date("Y-m-d H:i:s");
        
        $dataread = New DataRead();
        $dataWrite = New DataWrite();
        
        $count = 0;
        
        //find the member details through th 
        //check whether the email and password exists
        $admin_login = $dataread->admin_login($mycon, $username, $password);
        

        if(!$admin_login)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Username or password combination is wrong!
                </div>";
            return;
        }
       
        
        
        
        createCookie("userid",$admin_login['admin_id']);
        createCookie("userlogin","YES");
        createCookie("adminlogin", "NO");
        createCookie("username",$admin_login['username']);
        
         echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> We are preparing your dashboard, please wait...
                    </div>
                    <script type-'text/javascript'>
                    window.setTimeout(function(){
                document.location.href='../adminend/allapplications.php';
            },2000);
                </script>";
        return;
        
    }

}



?>