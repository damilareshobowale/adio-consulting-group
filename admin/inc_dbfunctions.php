<?php
    require_once("config.php");




class DataWrite
{   
	//create the useracccount
    function application_add($mycon, $firstname, $surname, $phonenumber, $email, $coverletter, $passportname, $resumename)//
    {
      $thedate = date("Y-m-d H:i:s");
      $sql = "INSERT INTO `applications` SET `firstname` = :firstname
          ,`surname` = :surname
          ,`phonenumber` = :phonenumber
          ,`email` = :email
          ,`coverletter` = :coverletter
          ,`passportname` = :passportname
          ,`resumename` = :resumename
          ,`createdon` = :createdon
          ,`status` = :status";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":firstname", $firstname, PDO::PARAM_STR);
      $myrec->bindValue(":surname", $surname,PDO::PARAM_STR);
      $myrec->bindValue(":phonenumber", $phonenumber,PDO::PARAM_STR);
      $myrec->bindValue(":email", $email,PDO::PARAM_STR);
      $myrec->bindValue(":coverletter", $coverletter,PDO::PARAM_STR);
      $myrec->bindValue(":passportname", $passportname,PDO::PARAM_STR);
      $myrec->bindValue(":resumename", $resumename,PDO::PARAM_STR);
      $myrec->bindValue(":createdon", $thedate,PDO::PARAM_STR);
      $myrec->bindValue(":status", '5',PDO::PARAM_STR);// status set to 0 to show Passive member
      $myrec->execute();
      
      if ($myrec->rowCount() < 1) return false;
      
      return $mycon->lastInsertId();
      
    }
}

class DataRead {

	 //function to get the list of all the countries
    function application_count($mycon)
    {
        $sql = "SELECT * FROM `applications`";
        $myrec = $mycon->prepare($sql);
        $myrec->execute();

        return $myrec->rowCount();
    }

    //check if username exists
    function application_getbyemail($mycon,$email)
    {
        $sql = "SELECT * FROM `applications` WHERE `email` = :email LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":email", $email);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //check if username exists
    function admin_login($mycon,$username, $password)
    {
        $sql = "SELECT * FROM `admins` WHERE `username` = :username && `password` = :password LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":username", $username);
        $myrec->bindValue(":password", $password);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //check if username exists
    function applications_get($mycon)
    {
        $sql = "SELECT * FROM `applications`";
        $myrec = $mycon->prepare($sql);
        $myrec->execute();
        
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
}


?>