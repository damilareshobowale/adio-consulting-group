<?php
include_once('../admin/inc_dbfunctions.php');


$mycon = databaseConnect();
        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
//get the list of all the applications
$applications_get = $dataRead->applications_get($mycon);


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>List of all applications</title>
    <!-- Bootstrap Core CSS -->
    <link href="bootstrap.css" rel="stylesheet">
    <style type="text/css">
    	.table 
    	{
    		border: 1px solid #444444;
    		padding: 20px;
    		margin: 20px;
    	}

    	.table tbody th {
    		border: 1px solid #444444;
    		margin:15%;
    	}

    	div .container .center h2 
    	{
    		margin: 10px;
    	}

    	.proceedtocheckout
    	{
    		margin: 10px;
    		font-weight: bold;
    	}
    </style>
    

</script>
</head>
    <body>
        <!-- Page Content -->
        <div class="container center">
            <h2 class="text-center">Adio Consulting Group <br /><br />
                    Application Lists </h2> <br /><br/>
            
            <table class="table">
                <thead>
                    <th>
                        Fullname
                    </th>
                    <th>
                        Phonenumber
                    </th>
                    <th>
                        Email
                    </th>
                    <th>
                        Resume
                    </th>
                    <th>
                        Passport
                    </th>
                </thead>
                <tbody>
                    <?php

                            foreach ($applications_get as $key) {
                           
                    ?>
                    <tr>
                        <td><?php echo $key['firstname']. " ". $key['surname'] ?></td>
                        <td> <?php echo $key['phonenumber'] ?></td>
                        <td>  <?php echo $key['email'] ?></td>
                        <td>  <?php echo $key['resume'] ?></td>
                        <td>  <?php echo $key['passport'] ?></td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>

    <!-- jQuery -->
    <script src="jquery.js"></script>
    <script src="custom.js"></script>


</body>
</html>