<?php
include_once('../admin/config.php');
include_once('../admin/inc_dbfunctions.php');

$mycon  = databaseConnect();
$dataRead = New DataRead();

//check if the appliation is more than 4
$application_count = $dataRead->application_count($mycon);


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Adio Consulting Group Recruitment</title>
    <!-- Bootstrap Core CSS -->
    <link href="../bootstrap.css" rel="stylesheet">
    <style type="text/css">
        .table 
        {
            border: 1px solid #444444;
            padding: 20px;
            margin: 20px;
        }

        .table tbody th {
            border: 1px solid #444444;
            margin:15%;
        }

        div .container .center h2 
        {
            margin: 10px;
        }

        .proceedtocheckout
        {
            margin: 10px;
            font-weight: bold;
        }
    </style>
    

</script>
</head>
    <body>
        <!-- Page Content -->
        <div class="container center">
            <h2 class="text-center">Adio Consulting Group <br /><br />
                    Back End Office </h2> <br /><br/>
          
            <hr />
            
            <form action="../admin/actionmanager.php" method="post" id="adminlogin">
               
                <div class="row">
                    <div class="col-md-6 form-group firstnamediv">
                        <label for="firstname">Username:</label>
                        <input type="text" name="username" id="username" class="form-control">
                    </div>
                    <div class="col-md-6 form-group lastnamediv">
                        <label for="password">password:</label>
                        <input type="password" name="password" id="password" class="form-control">
                    </div>
                </div>
                 <div class="row">
                    <div class="col-md-12 form-group text-center">
                        <button type="submit" class="btn btn-success" id="adminloginbutton">Login</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                    </div>
                     <div id="result"></div>
                </div>
                <div class="row">
                    <p>Login access</p> <br> 
                    <p>Username: admin </p> <br>
                    <p>Password: password </p>
                </div>
            </form>
    <!-- jQuery -->
    <script src="../jquery.js"></script>
    <script src="../custom.js"></script>


</body>
</html>