<?php
include_once('admin/config.php');
include_once('admin/inc_dbfunctions.php');

$mycon  = databaseConnect();
$dataRead = New DataRead();

//check if the appliation is more than 4
$application_count = $dataRead->application_count($mycon);


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Adio Consulting Group Recruitment</title>
    <!-- Bootstrap Core CSS -->
    <link href="bootstrap.css" rel="stylesheet">
    <style type="text/css">
    	.table 
    	{
    		border: 1px solid #444444;
    		padding: 20px;
    		margin: 20px;
    	}

    	.table tbody th {
    		border: 1px solid #444444;
    		margin:15%;
    	}

    	div .container .center h2 
    	{
    		margin: 10px;
    	}

    	.proceedtocheckout
    	{
    		margin: 10px;
    		font-weight: bold;
    	}
    </style>
    

</script>
</head>
    <body>
        <!-- Page Content -->
        <div class="container center">
            <h2 class="text-center">Adio Consulting Group <br /><br />
                    Software Engineer Recruitment </h2> <br /><br/>
            <h3>Application Status: 
            <?php

            if ($application_count < 4)
            {
            ?>
             <span style='color:green' id="status">Application Open</span></h3>
             <?php
         }
         else {
            ?>
            <span style='color: red' id="status">Application Closed</span></h3>
            <?php
         }
         ?>

            <hr />
            <?php

                if ($application_count < 4)
                {
            ?>
            <form action="admin/actionmanager.php" method="post" id="applyform">
               
                <div class="row">
                    <div class="col-md-6 form-group firstnamediv">
                        <label for="firstname">First Name:</label>
                        <input type="text" name="firstname" id="firstname" class="form-control">
                    </div>
                    <div class="col-md-6 form-group lastnamediv">
                        <label for="lastname">Surname:</label>
                        <input type="text" name="surname" id="surname" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label for="phonenumber">Phone Number:</label>
                        <input type="text" name="phonenumber" id="phonenumber" class="form-control">
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="email">Email Address:</label>
                        <input type="email" name="email" id="email" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-group">
                        <label for="coverleter">Cover Letter:</label>
                        <textarea class="form-control" rows="10" id="coverletter" name="coverletter"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-group">
                        <label for="passport">Passport Photograph:<small class="text-danger">(JPEG, max of 100KB only) </small></label>
                        <input type="file" name="passport" id="passport" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-group">
                        <label for="resume">Upload Resume: <small class="text-danger">(PDF, DOC or DOCX format, max of 2MB only) </small></label>
                        <input type="file" name="resume" id="resume" class="form-control">
                    </div>
                </div>
                 <div class="row">
                    <div class="col-md-12 form-group text-center">
                        <button type="submit" class="btn btn-success" id="applyformbutton">Submit Application</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                    </div>
                     <div id="result"></div>
                </div>
            </form>
            <?php
                }

            ?>

    <!-- jQuery -->
    <script src="jquery.js"></script>
    <script src="custom.js"></script>


</body>
</html>