//SUBMIT THE REGISTERATION FORM
$('#applyform').on("submit", function(event){
	$('#applyformbutton').html("Submiting...");
	$('.error').removeClass("has-error");


	event.preventDefault();

	 /* get some values from elements on the page: */
         var $form = $(this),
         	firstname = $form.find('input[name="firstname"]').val(),
         	surname = $form.find('input[name="surname"]').val(),
            phonenumber = $form.find('input[name="phonenumber"]').val(),
            email = $form.find('input[name="email"]').val(),
            coverletter = $form.find('textarea[name="coverletter"]').val(),
            passport = $form.find('input[name="passport"]').val(),
            resume = $form.find('input[name="resume"]').val(),
            url = $form.attr('action');

     //handle validations
     msg = '';
     var count = 0;
     if (firstname == '')
     {
     	$('#firstnamediv').addClass("has-error")
     	msg += '<br />First name is required';
     	count += 1;
     }
     if (surname == '')
     {
     	$('#lastnamediv').addClass("has-error")
     	msg += '<br />Last name is required';
     	count += 1;
     }
     if (email == '')
     {
     	$('#emaildiv').addClass("has-error")
     	msg += '<br />Email is required';
     	count += 1;
     }
     if (phonenumber == '')
     {
        $('#phonenumberdiv').addClass("has-error")
        msg += '<br />Phonenumber is required';
        count += 1;
     }
     
     

     if (msg != '')
     {
     	if (count == 1)
     	{
     		$('#result').html("<div class='alert alert-danger alert-dismissable'>" + 
	                                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" +
	                                            "<i class='fa fa-warning'></i> ** "+count+" error was found, please correct <br />" + msg +
	                                        "</div>");
     	}
     	else {
     		$('#result').html("<div class='alert alert-danger alert-dismissable'>" + 
	                                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" +
	                                            "<i class='fa fa-warning'></i> ** "+count+" errors were found, please correct <br />" + msg +
	                                        "</div>");
     	}
     	$('#applyformbutton').html("Submit Application");
     	return;
     }

    
    var formData = new FormData(this);
    formData.append("command", "apply");

        $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                $('#result').html(data);
                $('#applyformbutton').html("Submit Application");
            },
            error: function(data){
                $('#applyformbutton').html(data);
                $('#applyformbutton').html("Submit Application");
            }
        });
       return;                 

});


//LOGIN FORM
$('#adminlogin').on("submit", function(event){
    $('#adminloginbutton').html("Logging...");
    $('.error').removeClass("has-error");


    event.preventDefault();

     /* get some values from elements on the page: */
         var $form = $(this),
            username = $form.find('input[name="username"]').val(),
            password = $form.find('input[name="password"]').val(),
            url = $form.attr('action');

     //handle validations
     msg = '';
     var count = 0;
     if (username == '')
     {
        $('#firstnamediv').addClass("has-error")
        msg += '<br />Username is empty';
        count += 1;
     }
     if (password == '')
     {
        $('#lastnamediv').addClass("has-error")
        msg += '<br />Passwod is empty';
        count += 1;
     }
     
     

     if (msg != '')
     {
        if (count == 1)
        {
            $('#result').html("<div class='alert alert-danger alert-dismissable'>" + 
                                                "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" +
                                                "<i class='fa fa-warning'></i> ** "+count+" error was found, please correct <br />" + msg +
                                            "</div>");
        }
        else {
            $('#result').html("<div class='alert alert-danger alert-dismissable'>" + 
                                                "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" +
                                                "<i class='fa fa-warning'></i> ** "+count+" errors were found, please correct <br />" + msg +
                                            "</div>");
        }
        $('#adminloginbutton').html("Login");
        return;
     }

    
    var formData = new FormData(this);
    formData.append("command", "login");

        $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                $('#result').html(data);
        $('#adminloginbutton').html("Login");
                $('#applyformbutton').html("Login");
            },
            error: function(data){
                $('#applyformbutton').html(data);
                $('#applyformbutton').html("Login");
            }
        });
       return;                 

});